import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (true) {
            System.out.println("Enter two numbers.");
            System.out.println("This program will divide the two & give you the results.");
            System.out.println("Please do not enter 0 as the second number.");
            System.out.println(" ");
            int n1 = sc.nextInt();
            int n2 = sc.nextInt();
            try {
                int s = div(n1, n2);
                System.out.println("The result is: "+s);
                break;
            } catch (ArithmeticException e) {
                System.out.println(e.getMessage() + " error.");
                System.out.println("You have entered a zero as a number. Unable to divide by zero.");
                System.out.println("Please try again");
                System.out.println(" ");
            }
        }
    }

    public static int div(int n1, int n2) throws ArithmeticException {
        return n1 / n2;
    }
}