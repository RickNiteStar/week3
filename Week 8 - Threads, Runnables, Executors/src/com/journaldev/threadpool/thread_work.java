package com.journaldev.threadpool;                          // manage pool of working thread

public class thread_work implements Runnable {                  // thread model runnable class

    private String thread;

    public thread_work(String s){
        this.thread=s;
    }

    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName()+" Starting... Thread = "+thread);
        proc_comm();
        System.out.println(Thread.currentThread().getName()+" End.");
    }

    private void proc_comm() {
        try {
            Thread.sleep(5000);                                 // suspend thread for 5000
        } catch (InterruptedException e) {                     // exception error handling
            e.printStackTrace();
        }
    }

    @Override
    public String toString(){
        return this.thread;
    }
}