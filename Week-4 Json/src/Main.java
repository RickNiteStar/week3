import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;


public class Main {

    public static String vehiclesToJSON(Vehicles vehicles) {
        ObjectMapper mapper = new ObjectMapper();
        String v = "";
        try {
            v = mapper.writeValueAsString(vehicles);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }
        return v;
    }
    public static Vehicles JSONToVehicles(String v) {
        ObjectMapper mapper = new ObjectMapper();
        Vehicles vehicles = null;
        try {
            vehicles = mapper.readValue(v, Vehicles.class);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }
        return vehicles;
    }

    public static void main(String[] args) {
        Vehicles veh = new Vehicles();
        veh.setType("Truck");
        veh.setMake("Ford");
        veh.setModel("F150");
        veh.setYear("2018");
        veh.setColor("White");
        veh.setMiles(89686);

        String json = Main.vehiclesToJSON(veh);

        System.out.println(json);

        }
}
