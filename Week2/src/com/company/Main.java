package com.company;

import java.util.*;

public class Main {

    public static void main(String[] args) {
        // Creating things
        // Created A list, a queue, a set, and then a tree aka tree set

        List<Integer> list = new ArrayList<>();
        Queue<Integer> queue = new LinkedList<>();
        Set<Integer> set = new HashSet<>();
        TreeSet<Integer> treeSet = new TreeSet<>();

        // Listing of the three and some of their unique features

        int[] arr = {415,134,26,67,34,67,81,51,56,12,17,89};
        for(int val: arr){
            list.add(val);
            queue.add(val);
            set.add(val);
            treeSet.add(val);
        }
        System.out.println("List all items: "+list.toString());
        System.out.println("First item of the queue: "+queue.peek());
        System.out.println("Unique item in set: "+set.toString());
        System.out.println("Sorted unique items in set low to high: "+treeSet.toString());

        // Using the comparator interface

        list.sort(new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return o1.compareTo(o2);
            }
        });
        System.out.println(" ");
        System.out.println("Sorted the list low to high with Comparator: "+list.toString());
    }
}