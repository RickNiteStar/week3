//Program testing Junit...I have set it up to test 6 of the possible assert methods.

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;

public class EmployeeTest {
    Employee employee1, employee2;

    // First set up message1 and message2
    @Before
    public void setUp() {
        employee1 = new Employee();
        employee2 = new Employee("Jhon Smith", "123-A", "11-15-2005");
    }

    // Test all methods
    // Test Case 1: Test getEmployeeNumber method
    @Test
    public void testgetEmployeeNumber() {
        assertEquals("123-A", employee2.getEmployeeNumber());
    }

    // Test Case 2: Test isValidEmpNum method with invalid data
    @Test
    public void testisValidEmpNum() {
        assertFalse(employee2.isValidEmpNum("234-BB"));
    }

    // Test Case 3: Test isValidEmpNum method with valid data
    @Test
    public void tesValidEmpNum() {
        assertTrue(employee2.isValidEmpNum("234-B"));
    }

    // Test Case 4: Test getHireDate method
    @Test
    public void tessetHireDate() {
        employee1.setHireDate("2021-04-06");
        assertNotNull(employee1.getHireDate());
    }

    // Test Case 5: Test getHireDate method with null value
    @Test
    public void tessetHireDateNullValue() {
        employee1.setHireDate(null);
        assertNull(employee1.getHireDate());
    }

    // Test Case 6: Test getName method
    @Test
    public void testgetName() {
        assertNotSame("Not Equal", "Jhonnn Smith", employee2.getName());
    }
}